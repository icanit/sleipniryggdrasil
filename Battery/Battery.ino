
#include <Wire.h>

#define DEBUG 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif

#define SENSOR_PIN A0
#define DIVIDER_PIN A1

#define SMOOTH_READINGS 100
int readings[SMOOTH_READINGS];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

int readings2[SMOOTH_READINGS];      // the readings from the analog input
int readIndex2 = 0;              // the index of the current reading
int total2 = 0;                  // the running total
int average2 = 0;                // the average

#define BATTERY_SLAVE_DEVICE_ID 31

void setup() {
	Wire.begin(BATTERY_SLAVE_DEVICE_ID);
	Wire.onRequest(requestCallback);

	// Initialize digital pins as outputs
	pinMode(SENSOR_PIN, INPUT);
	pinMode(DIVIDER_PIN, INPUT);

#ifdef DEBUG
	Serial.begin(115200);
#endif // DEBUG
	DEBUG_PRINTLN("Battery started.");

	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		readings[thisReading] = 0;
		readings2[thisReading] = 0;
	}
}

void loop() {
	readSensor();
	readSensor2();
	delay(1);
}


void  readSensor() {
	total = total - readings[readIndex];
	readings[readIndex] = getDividerValue();
	total = total + readings[readIndex];
	readIndex = readIndex + 1;

	if (readIndex >= SMOOTH_READINGS) {
		readIndex = 0;
	}
	average = total / SMOOTH_READINGS;

}
void  readSensor2() {
	total2 = total2 - readings2[readIndex2];
	readings2[readIndex2] = getCurrentSensorValue();
	total2 = total2 + readings2[readIndex2];
	readIndex2 = readIndex2 + 1;

	if (readIndex2 >= SMOOTH_READINGS) {
		readIndex2 = 0;
	}
	average2 = total2 / SMOOTH_READINGS;

}
int getCurrentSensorValue() {
	return analogRead(SENSOR_PIN);
}
int getCurrentSensorValueAvg() {
	return average2;
}

int getDividerValue() {
	return analogRead(DIVIDER_PIN);
}
int getDividerValueAvg() {
	return average;
}


void requestCallback()
{
	DEBUG_PRINTLN("REQUEST");
	int input = getDividerValueAvg();
	int input2 = getCurrentSensorValueAvg();
	uint8_t buffer[4];
	buffer[0] = input >> 8;
	buffer[1] = input & 0xff;
	buffer[2] = input2 >> 8;
	buffer[3] = input2 & 0xff;
	Wire.write(buffer, 4);
}
