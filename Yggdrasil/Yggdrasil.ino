﻿#include <Wire.h>

//#define DEBUG 1
//#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif



//Transmission 1x
#define BRAKES_SLAVE_DEVICE_ID 11
#define MOTOR_SLAVE_DEVICE_ID 12
#define STEERING_SLAVE_DEVICE_ID 13
#define STEERING_BRAKES_SLAVE_DEVICE_ID 14

//Controls 2x
#define CONTROL_SLAVE_DEVICE_ID 21

//Sensors 3x
#define BATTERY_SLAVE_DEVICE_ID 31


int wheelPosition;
int brakesPosition;

int wheelCs;
int brakesCs;

int controlX;
int controlY;
int carSpeed;
int current;
int voltage;

int breakActive = 0;
int wheelCommand = 512;
int motorThrust = 0;

void setup()
{
	// Start I²C bus as master
	Wire.begin();

#ifdef DEBUG
	Serial.begin(115200);
#endif
	DEBUG_PRINTLN("Master started");
}


void loop() {
	TRACE_PRINTLN("Collect data...");
	collectData();

	TRACE_PRINTLN("Process data...");
	processData();

	TRACE_PRINTLN("Send data...");
	sendData();

	delay(500);
}

#pragma region Collect

void collectData() {
	collectControlData();
	collectMotorData();
	collectSteeringBrakesData();
}

void collectControlData() {
	int data[6];
	requestDataFromSlave(CONTROL_SLAVE_DEVICE_ID, data, 6);
	controlY = data[0];
	controlX = data[1];
}

void collectMotorData() {
	int data[1];
	requestDataFromSlave(MOTOR_SLAVE_DEVICE_ID, data, 1);
	carSpeed = data[0];
}

void collectSteeringBrakesData() {
	int data[2];
	requestDataFromSlave(STEERING_BRAKES_SLAVE_DEVICE_ID, data, 1);
	wheelPosition = data[0];
	wheelCs= data[1];
	brakesPosition = data[2];
	brakesCs = data[3];
}
#pragma endregion

#pragma region Process

void processData() {
	processControlData();
}

void processControlData() {

	int y = controlY;
	if (y > 512 - 50 && y < 512 + 50) {
		y = 512;
	}
	breakActive = y < 512 ? 1024 : 0;
	motorThrust = y > 512 ? (y - 512) * 2 : 0;

	int x = controlX - 3;
	if (x > 512 - 50 && x < 512 + 50) {
		x = 512;
	}
	wheelCommand = x;


	DEBUG_PRINT("\tX: \t");
	DEBUG_PRINT(controlX);
	DEBUG_PRINT(";\tB: \t");
	DEBUG_PRINT(breakActive);
	DEBUG_PRINTLN(";");
}
#pragma endregion

#pragma region Deprecated
void collectSteeringData() {
	int data[1];
	requestDataFromSlave(STEERING_SLAVE_DEVICE_ID, data, 1);
	wheelPosition = data[0];
}

void collectBrakesData() {
	int data[1];
	requestDataFromSlave(BRAKES_SLAVE_DEVICE_ID, data, 1);
	brakesPosition = data[0];
}
void sendDataToSteering() {
	int data[1];

	data[0] = wheelCommand;
	sendDataToSlave(STEERING_SLAVE_DEVICE_ID, data, 1);
}
void sendDataToBrakes() {
	int data[1];
	data[0] = breakActive;
	sendDataToSlave(BRAKES_SLAVE_DEVICE_ID, data, 1);
}
#pragma endregion

#pragma region Send
void sendData() {
	sendDataToSteeringBrakes();
	sendDataToMotor();
}

void sendDataToMotor() {
	int data[3];
	data[0] = motorThrust;
	data[1] = breakActive > 0 ? 1 : 0;
	data[2] = breakActive > 0 ? 1 : 0;
	sendDataToSlave(MOTOR_SLAVE_DEVICE_ID, data, 3);
}

void sendDataToSteeringBrakes() {
	int data[2];

	data[0] = wheelCommand;
	data[1] = breakActive;
	sendDataToSlave(STEERING_BRAKES_SLAVE_DEVICE_ID, data, 2);
}
#pragma endregion

#pragma region Utils
void sendDataToSlave(byte deviceId, int data[], uint8_t len) {
	// Send two bytes to slave.
	Wire.beginTransmission(deviceId);
	uint8_t buffer[12];
	for (int i = 0; i < len; i++) {
		buffer[i * 2] = (data[i] >> 8);
		buffer[i * 2 + 1] = (data[i] & 255);
	}
	Wire.write(buffer, len * 2);
	Wire.endTransmission();
}

void requestDataFromSlave(byte deviceId, int data[], uint8_t len) {

	// Request data from slave.
	//Wire.beginTransmission(deviceId);
	int available = Wire.requestFrom(deviceId, (uint8_t)(len * 2));
	TRACE_PRINT("Available: ");
	TRACE_PRINTLN(available);

	if (available == (len * 2))
	{

		for (int i = 0; i < len; i++) {
			int receivedValue = Wire.read() << 8 | Wire.read();
			data[i] = receivedValue;
			TRACE_PRINT(i);
			TRACE_PRINT(": ");
			TRACE_PRINTLN(receivedValue);
		}
	}
	else
	{
		DEBUG_PRINT("Unexpected number of bytes received: ");
		DEBUG_PRINTLN(available);
	}
	//Wire.endTransmission();
}

#pragma endregion
