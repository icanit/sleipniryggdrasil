

#include <Wire.h>

#define DEBUG 1
//#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#define BRAKEVCC 0
#define CW   1
#define CCW  2
#define BRAKEGND 3
#define CS_THRESHOLD 100


#define SENSOR_PIN A0

#define IN_A_PIN  7 // INA: Clockwise input
#define IN_B_PIN 6 // INB: Counter-clockwise input
#define PWM_PIN 9 // PWM input
#define CS_PIN A7 // CS: Current sense ANALOG input
#define EN_PIN 3 // EN: Status of switches output (Analog pin)

#define CENTER_POSITION 512
#define MOVE_RANGE 350
#define STEERING_ELIPSILON 50

#define SAFE_PAUSE_DELAY 200
#define MOTOR_PWM 255

#define STEERING_SLAVE_DEVICE_ID 13

#define SMOOTH_READINGS 100
int readings[SMOOTH_READINGS];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average


int positionToMove;
bool makeMove = false;
bool safePause = false;

uint8_t motorSpeed = 0;
uint8_t direct = CW;


void setup() {
	Wire.begin(STEERING_SLAVE_DEVICE_ID);
	Wire.onReceive(receiveCallback);
	Wire.onRequest(requestCallback);

	// Initialize digital pins as outputs
	pinMode(IN_A_PIN, OUTPUT);
	pinMode(IN_B_PIN, OUTPUT);
	pinMode(PWM_PIN, OUTPUT);
	pinMode(EN_PIN, OUTPUT);

	pinMode(CS_PIN, INPUT);
	// Initialize 
	motorOff();

#ifdef DEBUG
	Serial.begin(115200);
#endif // DEBUG

	DEBUG_PRINTLN("Steering started.");

	digitalWrite(EN_PIN, HIGH);

	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		readings[thisReading] = 0;
	}
}

void motorOff()
{
	digitalWrite(IN_A_PIN, LOW);
	digitalWrite(IN_B_PIN, LOW);
	analogWrite(PWM_PIN, 0);
}

void loop() {
	if (safePause) {
		motorOff();
		delay(SAFE_PAUSE_DELAY);
		safePause = false;
		DEBUG_PRINTLN("SAFE_PAUSE");
	}
	if (makeMove) {
		int currPos = getSteeringPositionValue();
		if ((currPos < CENTER_POSITION - MOVE_RANGE) && (currPos > CENTER_POSITION + MOVE_RANGE)) {//�� �������� �� ������� �����
			motorOff();
			makeMove = false;
			DEBUG_PRINTLN("LIMIT_STOP");
		}
		else if (direct == CCW && currPos > positionToMove) {
			motorGo(CCW, MOTOR_PWM);
		}
		else if (direct == CW && currPos < positionToMove) {
			motorGo(CW, MOTOR_PWM);
		}
		else {//�� �������� ����
			motorOff();
			makeMove = false;
			DEBUG_PRINTLN("STOP");
		}
	}
	else {
		motorOff();
	}
	readSensor();
}

unsigned long csLastTime = 0;

void  readSensor() {
	unsigned long curInterval = millis();

	if (curInterval - csLastTime > 1000) {
		csLastTime = curInterval;

		total = total - readings[readIndex];
		readings[readIndex] = getCurrentSensorValue();
		total = total + readings[readIndex];
		readIndex = readIndex + 1;

		if (readIndex >= SMOOTH_READINGS) {
			readIndex = 0;
		}
		average = total / SMOOTH_READINGS;
	}
}
int getCurrentSensorValue() {
	return analogRead(CS_PIN);
}
int getCurrentSensorValueAvg() {
	return average;
}
int getSteeringPositionValue() {
	return analogRead(SENSOR_PIN);
}

void setSteeringPositionToMove(long newPos) {
	newPos = 1023 - newPos;
	positionToMove = (newPos - 512) * (2 * MOVE_RANGE) / 1024.0 + CENTER_POSITION;

	TRACE_PRINT("New pos: \t");
	TRACE_PRINTLN(newPos);
	TRACE_PRINT("Position to move: \t");
	TRACE_PRINTLN(positionToMove);

	int currPos = getSteeringPositionValue();
	int newDirect = currPos > positionToMove ? CCW : CW;

	if (abs(currPos - positionToMove) > STEERING_ELIPSILON) {
		DEBUG_PRINT("Making move from: \t");
		DEBUG_PRINT(currPos);
		DEBUG_PRINT("\t to: \t");
		DEBUG_PRINTLN(positionToMove);

		if (makeMove && newDirect != direct) {
			safePause = true;
		}

		direct = newDirect;
		makeMove = true;
	}
}

boolean csWarn = false;

void motorGo(uint8_t direct, uint8_t pwm)
{
	// Set inA
	if (direct == CW) {
		digitalWrite(IN_A_PIN, HIGH);
		digitalWrite(IN_B_PIN, LOW);
	}
	else {
		digitalWrite(IN_A_PIN, LOW);
		digitalWrite(IN_B_PIN, HIGH);
	}

	if (getCurrentSensorValueAvg() < CS_THRESHOLD) {
		analogWrite(PWM_PIN, pwm);
		csWarn = false;
	}
	else if (!csWarn) {
		DEBUG_PRINTLN("CS_THRESHOLD");
		csWarn = true;
	}
}

void receiveCallback(int aCount)
{
	TRACE_PRINTLN("Command recieved.");
	if (aCount == 2)
	{
		int receivedValue = Wire.read() << 8 | Wire.read();
		setSteeringPositionToMove(receivedValue);

		TRACE_PRINT("\tCOM: \t");
		TRACE_PRINT(receivedValue);
		TRACE_PRINTLN(";");
	}
	else
	{
		DEBUG_PRINT("Unexpected number of bytes received: ");
		DEBUG_PRINTLN(aCount);
	}
}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int input = (getSteeringPositionValue() - CENTER_POSITION) * 1024 / (2 * MOVE_RANGE) + 512;
	int input2 = getCurrentSensorValueAvg();

	uint8_t buffer[4];
	buffer[0] = input >> 8;
	buffer[1] = input & 0xff;
	buffer[2] = input2 >> 8;
	buffer[3] = input2 & 0xff;
	Wire.write(buffer, 4);

	TRACE_PRINT("\tB: \t");
	TRACE_PRINT(input);
	TRACE_PRINT("\tC: \t");
	TRACE_PRINT(input2);
	TRACE_PRINTLN(";");
}
