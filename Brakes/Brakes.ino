
#include <Wire.h>

#define DEBUG 1
//#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#define CW   1
#define CCW  2

#define CS_THRESHOLD 100

#define SENSOR_PIN A0

#define IN_A_PIN 7 // INA: Clockwise input
#define IN_B_PIN 6 // INB: Counter-clockwise input
#define PWM_PIN 9 // PWM input
#define CS_PIN A7 // CS: Current sense ANALOG input
#define EN_PIN 3 // EN: Status of switches output (Analog pin)

#define MIN_BRAKE_POSITION 620
#define MAX_BRAKE_POSITION 970
#define BRAKE_ELIPSILON_MIN 150
#define BRAKE_ELIPSILON_MAX 20
#define BRAKE_ELIPSILON 20

#define SAFE_PAUSE_DELAY 200
#define MOTOR_PWM 255

#define BRAKES_SLAVE_DEVICE_ID 11

#define SMOOTH_READINGS 100
int readings[SMOOTH_READINGS];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average


int positionToMove;
bool makeMove = false;
bool safePause = false;

uint8_t motorSpeed = 0;
uint8_t direct = CW;

void setup() {
	Wire.begin(BRAKES_SLAVE_DEVICE_ID);
	Wire.onReceive(receiveCallback);
	Wire.onRequest(requestCallback);

	// Initialize digital pins as outputs
	pinMode(IN_A_PIN, OUTPUT);
	pinMode(IN_B_PIN, OUTPUT);
	pinMode(PWM_PIN, OUTPUT);
	pinMode(EN_PIN, OUTPUT);

	pinMode(CS_PIN, INPUT);
	// Initialize braked
	motorOff();

#ifdef DEBUG
	Serial.begin(115200);
#endif // DEBUG
	DEBUG_PRINTLN("Brakes started.");

	digitalWrite(EN_PIN, HIGH);


	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		readings[thisReading] = 0;
	}
}

void loop() {
	if (safePause) {
		motorOff();
		delay(SAFE_PAUSE_DELAY);
		safePause = false;
		DEBUG_PRINTLN("SAFE_PAUSE");
	}
	if (makeMove) {
		int currPos = getBrakesPositionValue();
		if (currPos <MIN_BRAKE_POSITION &&currPos >MAX_BRAKE_POSITION) {//�� �������� �� ������� �����
			motorOff();
			makeMove = false;
			DEBUG_PRINTLN("LIMIT_STOP");
		}
		else if (direct == CW && currPos > positionToMove + BRAKE_ELIPSILON_MIN) {
			motorGo(CW, MOTOR_PWM);
			//DEBUG_PRINTLN("CW");
		}
		else if (direct == CCW && currPos < positionToMove - BRAKE_ELIPSILON_MAX) {
			motorGo(CCW, MOTOR_PWM);
			//DEBUG_PRINTLN("CCW");
		}
		else {//�� �������� ����
			motorOff();
			makeMove = false;
			DEBUG_PRINTLN("STOP");
		}
	}
	else {
		motorOff();
	}
	readSensor();
}

void motorOff()
{
	digitalWrite(IN_A_PIN, LOW);
	digitalWrite(IN_B_PIN, LOW);
	analogWrite(PWM_PIN, 0);
}

boolean csWarn = false;

void motorGo(uint8_t direct, uint8_t pwm)
{
	// Set inA
	if (direct == CCW) {
		digitalWrite(IN_A_PIN, HIGH);
		digitalWrite(IN_B_PIN, LOW);
	}
	else {
		digitalWrite(IN_A_PIN, LOW);
		digitalWrite(IN_B_PIN, HIGH);
	}

	if (getCurrentSensorValueAvg() < CS_THRESHOLD) {
		analogWrite(PWM_PIN, pwm);
		csWarn = false;
	}
	else if (!csWarn) {
		DEBUG_PRINTLN("CS_THRESHOLD");
		csWarn = true;
	}
}


unsigned long csLastTime = 0;

void  readSensor() {
	unsigned long curInterval = millis();

	if (curInterval - csLastTime > 1000) {
		csLastTime = curInterval;

		total = total - readings[readIndex];
		readings[readIndex] = getCurrentSensorValue();
		total = total + readings[readIndex];
		readIndex = readIndex + 1;

		if (readIndex >= SMOOTH_READINGS) {
			readIndex = 0;
		}
		average = total / SMOOTH_READINGS;
	}
}
int getCurrentSensorValue() {
	return analogRead(CS_PIN);
}
int getCurrentSensorValueAvg() {
	return average;
}
int getBrakesPositionValue() {
	return analogRead(SENSOR_PIN);
}

void setBrakesPositionToMove(long newPos) {


	positionToMove = newPos * (MAX_BRAKE_POSITION - MIN_BRAKE_POSITION) / 1024.0 + MIN_BRAKE_POSITION;


	TRACE_PRINT("New pos: \t");
	TRACE_PRINTLN(newPos);
	TRACE_PRINT("Position to move: \t");
	TRACE_PRINTLN(positionToMove);

	int currPos = getBrakesPositionValue();
	int newDirect = currPos > positionToMove ? CW : CCW;

	if (abs(currPos - positionToMove) > BRAKE_ELIPSILON) {
		DEBUG_PRINT("Making move from: \t");
		DEBUG_PRINT(currPos);
		DEBUG_PRINT("\t to: \t");
		DEBUG_PRINTLN(positionToMove);
		if (makeMove && newDirect != direct) {
			safePause = true;
		}
		direct = newDirect;
		makeMove = true;
	}
}

void receiveCallback(int aCount)
{
	TRACE_PRINTLN("Command recieved.");
	if (aCount == 2)
	{
		int receivedValue = Wire.read() << 8;
		receivedValue |= Wire.read();

		setBrakesPositionToMove(receivedValue);

		TRACE_PRINT("\tCOM: \t");
		TRACE_PRINT(receivedValue);
		TRACE_PRINTLN(";");
	}
	else
	{
		DEBUG_PRINT("Unexpected number of bytes received: ");
		DEBUG_PRINTLN(aCount);
	}
}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int pos = getBrakesPositionValue();
	if (pos > MAX_BRAKE_POSITION - BRAKE_ELIPSILON_MAX) {
		pos = MAX_BRAKE_POSITION;
	}
	if (pos < MIN_BRAKE_POSITION + BRAKE_ELIPSILON_MIN) {
		pos = MIN_BRAKE_POSITION;
	}
	int input = (pos - MIN_BRAKE_POSITION) * 1024.0 / (MAX_BRAKE_POSITION - MIN_BRAKE_POSITION);
	int input2 = getCurrentSensorValueAvg();
	uint8_t buffer[4];
	buffer[0] = input >> 8;
	buffer[1] = input & 0xff;
	buffer[2] = input2 >> 8;
	buffer[3] = input2 & 0xff;
	Wire.write(buffer, 4);

	TRACE_PRINT("\tB: \t");
	TRACE_PRINT(input);
	TRACE_PRINT("\tC: \t");
	TRACE_PRINT(input2);
	TRACE_PRINTLN(";");
}
