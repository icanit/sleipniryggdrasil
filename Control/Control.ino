#include <Wire.h>

#define DEBUG 1
#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#define PIN_X_1 A1
#define PIN_Y_1 A0
#define PIN_X_2 A3
#define PIN_Y_2 A2
#define PIN_BUTTON_1 2
#define PIN_BUTTON_2 3

//#define SERIAL

#define CONTROL_SLAVE_DEVICE_ID 21

void setup() {
	Wire.begin(CONTROL_SLAVE_DEVICE_ID);
	Wire.onRequest(requestCallback);

	pinMode(PIN_X_1, INPUT);
	pinMode(PIN_Y_1, INPUT);

	pinMode(PIN_BUTTON_1, INPUT_PULLUP);

	pinMode(PIN_X_2, INPUT);
	pinMode(PIN_Y_2, INPUT);

	pinMode(PIN_BUTTON_2, INPUT_PULLUP);

#ifdef DEBUG
	Serial.begin(115200);
#endif // DEBUG

	DEBUG_PRINTLN("Control started.");
}

void loop() {

}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int xPosition1 = analogRead(PIN_X_1);
	int yPosition1 = analogRead(PIN_Y_1);
	int buttonState1 = digitalRead(PIN_BUTTON_1);
	int xPosition2 = analogRead(PIN_X_2);
	int yPosition2 = analogRead(PIN_Y_2);
	int buttonState2 = digitalRead(PIN_BUTTON_2);

	TRACE_PRINT("\tX: \t");
	TRACE_PRINT(xPosition1);
	TRACE_PRINT(";\tY: \t");
	TRACE_PRINT(yPosition1);
	TRACE_PRINTLN(";");

	uint8_t buffer[12];
	buffer[0] = xPosition1 >> 8;
	buffer[1] = xPosition1 & 0xff;
	buffer[2] = yPosition1 >> 8;
	buffer[3] = yPosition1 & 0xff;
	buffer[4] = buttonState1 >> 8;
	buffer[5] = buttonState1 & 0xff;
	buffer[6] = xPosition2 >> 8;
	buffer[7] = xPosition2 & 0xff;
	buffer[8] = yPosition2 >> 8;
	buffer[9] = yPosition2 & 0xff;
	buffer[10] = buttonState2 >> 8;
	buffer[11] = buttonState2 & 0xff;
	Wire.write(buffer, 12);
}
