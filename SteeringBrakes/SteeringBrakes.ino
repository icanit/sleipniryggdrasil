
#include <motordriver_4wd.h>
#include <Wire.h>

#pragma region DEFINES

#define DEBUG 1
//#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#define STEERING_BRAKES_SLAVE_DEVICE_ID 14

#define FWD   0x00
#define REV  0x01

#define STEERING_SENSOR_PIN A2
#define BRAKES_SENSOR_PIN A3

#define STEERING_CS_PIN A0 // CS: Current sense ANALOG input
#define BREAKES_CS_PIN A1 // CS: Current sense ANALOG input

#define STEERING_CENTER_POSITION 512
#define STEERING_MOVE_RANGE 350
#define STEERING_ELIPSILON 50
#define STEERING_CS_SAFE_PAUSE_DELAY 200
#define STEERING_DIRECTION_SAFE_PAUSE_DELAY 200
#define STEERING_MOTOR_SPEED_MAX 255
#define STEERING_MOTOR_SPEED_MIN 127
#define STEERING_MOTOR_SPEED_ACCELERATION 255
#define STEERING_CS_THRESHOLD 200 //15A
#define STEERING_CS_ZERO 510;


#define BRAKES_POSITION_MIN 750
#define BRAKES_POSITION_MAX 930
#define BRAKES_ELIPSILON_MIN 150
#define BRAKES_ELIPSILON_MAX 20
#define BRAKES_ELIPSILON 20
#define BRAKES_CS_SAFE_PAUSE_DELAY 200
#define BRAKES_DIRECTION_SAFE_PAUSE_DELAY 200
#define BRAKES_MOTOR_SPEED_MAX 255
#define BRAKES_MOTOR_SPEED_MIN 127
#define BRAKES_MOTOR_SPEED_ACCELERATION 255
#define BRAKES_CS_THRESHOLD 170 //15A
#define BRAKES_CS_ZERO 510;

#define SMOOTH_READINGS 100

#pragma endregion

#pragma region SMOOTHNESS

int steeringCsReadings[SMOOTH_READINGS];      // the readings from the analog input
int steeringCsReadIndex = 0;              // the index of the current reading
int steeringCsTotal = 0;                  // the running total
int steeringCsAverage = 0;                // the average

int brakesCsReadings[SMOOTH_READINGS];      // the readings from the analog input
int brakesCsReadIndex = 0;              // the index of the current reading
int brakesCsTotal = 0;                  // the running total
int brakesCsAverage = 0;                // the average

#pragma endregion

#pragma region Vars
//steering
unsigned int	steeringPositionToMove = STEERING_CENTER_POSITION;
boolean			steeringMovementActive = false;
unsigned long	steeringSafePauseUntil = 0;
unsigned char	steeringMotorSpeed = 0;
unsigned char	steeringMotorDirection = FWD;
unsigned long	steeringCsLastTime = 0;
unsigned int	steeringCurrentPosition = STEERING_CENTER_POSITION;

//brakes
unsigned int	brakesPositionToMove = BRAKES_POSITION_MIN;
boolean			brakesMovementActive = false;
unsigned long	brakesSafePauseUntil = 0;
unsigned char	brakesMotorSpeed = 0;
unsigned char	brakesMotorDirection = FWD;
unsigned long	brakesCsLastTime = 0;
unsigned int	brakesCurrentPosition = BRAKES_POSITION_MIN;


#pragma endregion

void setup() {
	Wire.begin(STEERING_BRAKES_SLAVE_DEVICE_ID);
	Wire.onReceive(receiveCallback);
	Wire.onRequest(requestCallback);

	MOTOR.init();

	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		steeringCsReadings[thisReading] = 0;
	}
	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		brakesCsReadings[thisReading] = 0;
	}

#ifdef DEBUG
	Serial.begin(57600);
#endif // DEBUG

	DEBUG_PRINTLN("SteeringBrakes started.");

}

unsigned long lastCycleTime = 0;
unsigned long currentCycleTime = 0;
unsigned long cycleDeltaMillis = 0;
// the loop function runs over and over again until power down or reset
void loop() {
	currentCycleTime = millis();
	cycleDeltaMillis = currentCycleTime - lastCycleTime;

	updateBrakesSensor();
	updateSteeringSensor();

	processSteeringMovement(cycleDeltaMillis);
	processBrakesMovement(cycleDeltaMillis);
}

//����� �������� ���������
int calculateMovementSpeed(unsigned char currentSpeed, unsigned char maxSpeed, unsigned char accelerationPerSecond, unsigned long deltaTimeMillis) {
	if(currentSpeed<maxSpeed){//���������
		int newSpeed = currentSpeed + accelerationPerSecond*deltaTimeMillis / 1000;
		if (newSpeed > maxSpeed) {
			newSpeed = maxSpeed;
		}
		return newSpeed;
	}
	else {
		return maxSpeed;
	}
}

float calucalteCurrent(unsigned int curent) {
	return (((curent) * 75.7576) / 1024);
}
#pragma region Steering
void updateSteeringSensor() {
	unsigned long curInterval = millis();

	if (curInterval - steeringCsLastTime > 2) {
		steeringCsLastTime = curInterval;

		steeringCsTotal = steeringCsTotal - steeringCsReadings[steeringCsReadIndex];
		steeringCsReadings[steeringCsReadIndex] = getSteeringCurrentSensorValue();
		steeringCsTotal = steeringCsTotal + steeringCsReadings[steeringCsReadIndex];
		steeringCsReadIndex = steeringCsReadIndex + 1;

		if (steeringCsReadIndex >= SMOOTH_READINGS) {
			steeringCsReadIndex = 0;
		}
		steeringCsAverage = steeringCsTotal / SMOOTH_READINGS;
	}
	steeringCurrentPosition = getSteeringPositionValue();
}
int getSteeringCurrentSensorValue() {
	return analogRead(STEERING_CS_PIN) -STEERING_CS_ZERO ;
}
int getSteeringPositionValue() {
	return analogRead(STEERING_SENSOR_PIN);
}
void setSteeringPositionToMove(long newPos) {
	newPos = 1023 - newPos;
	steeringPositionToMove = (newPos - 512) * (2 * STEERING_MOVE_RANGE) / 1024.0 + STEERING_CENTER_POSITION;
	TRACE_PRINT("New steering pos: \t");
	TRACE_PRINTLN(newPos);
	TRACE_PRINT("Position to move: \t");
	TRACE_PRINTLN(steeringPositionToMove);

}
void processSteeringMovement(unsigned long deltaTimeMillis) {
	//��������� �� �����(����� ����� ���� ��-�� ��������� ������ �� ���� ��� ������  ��� ����� ����������� �������� ������)
	unsigned long now = millis();
	if (now < steeringSafePauseUntil) {
		stopSteeringMotor();
		return;
	}
	//��������� �� ����
	if (steeringCsAverage > STEERING_CS_THRESHOLD) {//��� �� ������
		steeringSafePauseUntil = now + STEERING_CS_SAFE_PAUSE_DELAY;
		DEBUG_PRINTLN("STEERING_CS_SAFE_PAUSE");
		DEBUG_PRINT("cs: \t");
		DEBUG_PRINTLN(calucalteCurrent(steeringCsAverage));
		stopSteeringMotor();
		return;
	}

	//��������� �� ����������� ���� ��������
	boolean steeringMovementActiveOld = steeringMovementActive;
	steeringMovementActive = abs(steeringCurrentPosition - steeringPositionToMove) > STEERING_ELIPSILON;
#ifdef DEBUG
	if (steeringMovementActiveOld != steeringMovementActive) {
		if (steeringMovementActive) {
			DEBUG_PRINTLN("STEERING_MOVE_START");
			DEBUG_PRINT("\tfrom: \t");
			DEBUG_PRINT(steeringCurrentPosition);
			DEBUG_PRINT("\tto: \t");
			DEBUG_PRINTLN(steeringPositionToMove);
		}
		else {
			DEBUG_PRINTLN("STEERING_MOVE_STOP");
			DEBUG_PRINT("\tat: \t");
			DEBUG_PRINTLN(steeringCurrentPosition);
		}	
	}
#endif // DEBUG
	if (!steeringMovementActive) {
		stopSteeringMotor();
		return;
	}

	//���������  ����� �����������
	int newDirect = steeringCurrentPosition < steeringPositionToMove ? FWD : REV;
	if (newDirect != steeringMotorDirection && steeringMovementActiveOld) {//���� �� ���������, �� �������� �����������, ������ �����
		steeringSafePauseUntil = now + STEERING_DIRECTION_SAFE_PAUSE_DELAY;
		steeringMotorDirection = newDirect;
		DEBUG_PRINTLN("STEERING_DIRECTION_SAFE_PAUSE");
		DEBUG_PRINT("\tfrom: \t");
		DEBUG_PRINT(steeringCurrentPosition);
		DEBUG_PRINT("\tto: \t");
		DEBUG_PRINTLN(steeringPositionToMove);
		stopSteeringMotor();
		return;
	}
	steeringMotorSpeed = calculateMovementSpeed(steeringMotorSpeed, STEERING_MOTOR_SPEED_MAX, STEERING_MOTOR_SPEED_ACCELERATION, cycleDeltaMillis);

	startSteeringMotor();
}
void stopSteeringMotor() {
	MOTOR.setStop1();
	steeringMotorSpeed = STEERING_MOTOR_SPEED_MIN;
}
void startSteeringMotor() {
	MOTOR.setSpeedDir1(steeringMotorSpeed, steeringMotorDirection ==FWD? DIRF:DIRR);
}
#pragma endregion

#pragma region Brakes
void updateBrakesSensor() {
	unsigned long curInterval = millis();

	if (curInterval - brakesCsLastTime > 2) {
		brakesCsLastTime = curInterval;

		brakesCsTotal = brakesCsTotal - brakesCsReadings[brakesCsReadIndex];
		brakesCsReadings[brakesCsReadIndex] = getBrakesCurrentSensorValue();
		brakesCsTotal = brakesCsTotal + brakesCsReadings[brakesCsReadIndex];
		brakesCsReadIndex = brakesCsReadIndex + 1;

		if (brakesCsReadIndex >= SMOOTH_READINGS) {
			brakesCsReadIndex = 0;
		}
		brakesCsAverage = brakesCsTotal / SMOOTH_READINGS;
	}

	brakesCurrentPosition = getBrakesPositionValue();
}
int getBrakesCurrentSensorValue() {
	return analogRead(BREAKES_CS_PIN)-BRAKES_CS_ZERO;
}
int getBrakesPositionValue() {
	return analogRead(BRAKES_SENSOR_PIN);
}

void setBrakesPositionToMove(long newPos) {
	brakesPositionToMove = newPos * (BRAKES_POSITION_MAX - BRAKES_POSITION_MIN) / 1024.0 + BRAKES_POSITION_MIN;
	TRACE_PRINT("New brakes pos: \t");
	TRACE_PRINTLN(newPos);
	TRACE_PRINT("Position to move: \t");
	TRACE_PRINTLN(brakesPositionToMove);

}
void processBrakesMovement(unsigned long deltaTimeMillis) {
	//��������� �� �����(����� ����� ���� ��-�� ��������� ������ �� ���� ��� ������  ��� ����� ����������� �������� ������)
	unsigned long now = millis();
	if (now < brakesSafePauseUntil) {
		stopBrakesMotor();
		return;
	}
	//��������� �� ����
	if (brakesCsAverage > BRAKES_CS_THRESHOLD) {//��� �� ������
		brakesSafePauseUntil = now + BRAKES_CS_SAFE_PAUSE_DELAY;
		DEBUG_PRINTLN("BRAKES_CS_SAFE_PAUSE");
		DEBUG_PRINT("\tcs: \t");
		DEBUG_PRINTLN(calucalteCurrent(brakesCsAverage));
		stopBrakesMotor();
		return;
	}

	//��������� �� ����������� ���� ��������
	boolean brakesMovementActiveOld = brakesMovementActive;
	brakesMovementActive = abs(brakesCurrentPosition - brakesPositionToMove) > BRAKES_ELIPSILON;
#ifdef DEBUG
	if (brakesMovementActiveOld != brakesMovementActive) {
		if (brakesMovementActive) {
			DEBUG_PRINTLN("BRAKES_MOVE_START");
			DEBUG_PRINT("\tfrom: \t");
			DEBUG_PRINT(brakesCurrentPosition);
			DEBUG_PRINT("\tto: \t");
			DEBUG_PRINTLN(brakesPositionToMove);
		}
		else {
			DEBUG_PRINTLN("BRAKES_MOVE_STOP");
			DEBUG_PRINT("\tat: \t");
			DEBUG_PRINTLN(brakesCurrentPosition);
		}
	}
#endif // DEBUG
	if (!brakesMovementActive) {
		stopBrakesMotor();
		return;
	}

	//���������  ����� �����������
	int newDirect = brakesCurrentPosition < brakesPositionToMove ? FWD : REV;
	if (newDirect != brakesMotorDirection && brakesMovementActiveOld) {//���� �� ���������, �� �������� �����������, ������ �����
		brakesSafePauseUntil = now + BRAKES_DIRECTION_SAFE_PAUSE_DELAY;
		brakesMotorDirection = newDirect;
		DEBUG_PRINTLN("BRAKES_DIRECTION_SAFE_PAUSE");
		DEBUG_PRINT("from: \t");
		DEBUG_PRINT(brakesCurrentPosition);
		DEBUG_PRINT("\tto: \t");
		DEBUG_PRINTLN(brakesPositionToMove);
		stopBrakesMotor();
		return;
	}
	brakesMotorSpeed = calculateMovementSpeed(brakesMotorSpeed, BRAKES_MOTOR_SPEED_MAX, BRAKES_MOTOR_SPEED_ACCELERATION, cycleDeltaMillis);

	startBrakesMotor();

}
void stopBrakesMotor() {
	MOTOR.setStop2();
	brakesMotorSpeed = BRAKES_MOTOR_SPEED_MIN;
}
void startBrakesMotor() {
	MOTOR.setSpeedDir2(brakesMotorSpeed, brakesMotorDirection == FWD ? DIRF : DIRR);
}
#pragma endregion

void receiveCallback(int aCount)
{
	TRACE_PRINTLN("Command recieved.");
	if (aCount == 4)
	{
		int receivedValue = Wire.read() << 8 | Wire.read();
		setSteeringPositionToMove(receivedValue);

		int receivedValue2 = Wire.read() << 8| Wire.read();
		setBrakesPositionToMove(receivedValue2);

		TRACE_PRINT("\ts:\t");
		TRACE_PRINT(receivedValue);
		TRACE_PRINT("\tb:\t");
		TRACE_PRINTLN(receivedValue2);

	}
	else
	{
		DEBUG_PRINT("Unexpected number of bytes received: ");
		DEBUG_PRINTLN(aCount);
	}
}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int steeringPos = (steeringCurrentPosition - STEERING_CENTER_POSITION) * 1024 / (2 * STEERING_MOVE_RANGE) + 512;
	int steeringCs = steeringCsAverage;

	int brakesPos = (brakesCurrentPosition - BRAKES_POSITION_MIN) * 1024.0 / (BRAKES_POSITION_MAX - BRAKES_POSITION_MIN);
	int brakesCs = brakesCsAverage;

	uint8_t buffer[8];
	buffer[0] = steeringPos >> 8;
	buffer[1] = steeringPos & 0xff;

	buffer[2] = steeringCs >> 8;
	buffer[3] = steeringCs & 0xff;

	buffer[4] = brakesPos >> 8;
	buffer[5] = brakesPos & 0xff;

	buffer[6] = brakesCs >> 8;
	buffer[7] = brakesCs & 0xff;

	Wire.write(buffer, 8);
}
