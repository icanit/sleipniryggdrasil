#include <Wire.h>

#define DEBUG 1
//#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#ifdef DEBUG

#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println (x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#endif

#define CW   1
#define CCW  2

#define SPEEDOMETER_PIN A0
#define REVERSE_PIN 2
#define DRIVE_PIN 3
#define HIGH_BRAKE_PIN 4
#define LOW_BRAKE_PIN 5
#define SERIAL

#define SAFE_PAUSE_DELAY 500

#define MOTOR_SLAVE_DEVICE_ID 12

#define SMOOTH_READINGS 100
int readings[SMOOTH_READINGS];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average


int moveSpeed;
bool highBrakes;
bool lowBrakes;

uint8_t direct = CW;
bool safePause = false;


void setup() {
	Wire.begin(MOTOR_SLAVE_DEVICE_ID);
	Wire.onReceive(receiveCallback);
	Wire.onRequest(requestCallback);

	pinMode(SPEEDOMETER_PIN, INPUT);

	pinMode(DRIVE_PIN, OUTPUT);
	pinMode(REVERSE_PIN, OUTPUT);
	pinMode(HIGH_BRAKE_PIN, OUTPUT);
	pinMode(LOW_BRAKE_PIN, OUTPUT);

	digitalWrite(DRIVE_PIN, LOW);
	digitalWrite(REVERSE_PIN, HIGH);
	digitalWrite(LOW_BRAKE_PIN, HIGH);
	digitalWrite(HIGH_BRAKE_PIN, HIGH);

#ifdef DEBUG
	Serial.begin(115200); 
#endif

	for (int thisReading = 0; thisReading < SMOOTH_READINGS; thisReading++) {
		readings[thisReading] = 0;
	}
}

void loop() {
	if (safePause) {
		analogWrite(DRIVE_PIN, LOW);
		delay(SAFE_PAUSE_DELAY);
		safePause = false;
	}
	if (!highBrakes && !lowBrakes) {
		analogWrite(DRIVE_PIN, moveSpeed / 2);
		digitalWrite(HIGH_BRAKE_PIN, LOW);
		digitalWrite(LOW_BRAKE_PIN, HIGH);
	}
	else 
	{
		analogWrite(DRIVE_PIN, LOW);
		if (highBrakes) {
			digitalWrite(HIGH_BRAKE_PIN, HIGH);
		}
		if (lowBrakes) {
			digitalWrite(LOW_BRAKE_PIN, LOW);
		}
	}
	readSensor();
}

unsigned long csLastTime = 0;

void  readSensor() {
	unsigned long curInterval = millis();

	if (curInterval - csLastTime > 1000) {
		csLastTime = curInterval;

		total = total - readings[readIndex];
		readings[readIndex] = getSpeedValue();
		total = total + readings[readIndex];
		readIndex = readIndex + 1;

		if (readIndex >= SMOOTH_READINGS) {
			readIndex = 0;
		}
		average = total / SMOOTH_READINGS;
	}
}

int getSpeedValue() {
	return analogRead(SPEEDOMETER_PIN);
}

int getSpeedValueAvg() {
	return average;
}

void setMotorTrust(int newPos) {
	moveSpeed = abs(newPos - 512);
	int newDirect = newPos > 512 ? CW : CCW;

	if (newDirect != direct) {
		safePause = true;
	}

	direct = newDirect;
}

void receiveCallback(int aCount)
{
	TRACE_PRINTLN("Command recieved.");

	if (aCount == 6)
	{
		int receivedValue = Wire.read() << 8;
		receivedValue |= Wire.read();

		lowBrakes = Wire.read() == 1;
		highBrakes = Wire.read() == 1;

		setMotorTrust(receivedValue);

		TRACE_PRINT("\S: \t");
		TRACE_PRINT(receivedValue);
		TRACE_PRINTLN(";");
	}
#ifdef SERIAL
	else
	{
		Serial.print("Unexpected number of bytes received: ");
		Serial.println(aCount);
	}
#endif
}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int input = getSpeedValueAvg();

	uint8_t buffer[2];
	buffer[0] = input >> 8;
	buffer[1] = input & 0xff;
	Wire.write(buffer, 2);

	TRACE_PRINT("\tS: \t");
	TRACE_PRINT(input);
	TRACE_PRINTLN(";");
}
