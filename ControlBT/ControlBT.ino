#include <Wire.h>
#include "SoftwareSerial.h"


#define DEBUG 1
#define TRACE 1

#ifdef DEBUG
#define DEBUG_PRINT(x)  Serial.print (x)
#define DEBUG_PRINTLN(x)  Serial.println(x)
#ifdef TRACE
#define TRACE_PRINT(x)  Serial.print (x)
#define TRACE_PRINTLN(x)  Serial.println (x)
#else
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x)
#define TRACE_PRINT(x)
#define TRACE_PRINTLN(x)
#endif

#define CONTROL_SLAVE_DEVICE_ID 21

#define    STX          0x02
#define    ETX          0x03
#define    ledPin       13
#define    SLOW         750                            // Datafields refresh rate (ms)
#define    FAST         250                             // Datafields refresh rate (ms)

SoftwareSerial mySerial(7, 6);                           // BlueTooth module: pin#2=TX pin#3=RX
byte cmd[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };                 // bytes received
byte buttonStatus = 0;                                  // first Byte sent to Android device
long previousMillis = 0;                                // will store last time Buttons status was updated
long sendInterval = FAST;                               // interval between Buttons status transmission (milliseconds)
String displayStatus = "xxxx";                          // message to Android device
int xPosition1 = 512;
int yPosition1 = 512;

int buttonState1 = 0;
int buttonState2 = 0;
int buttonState3 = 0;
int buttonState4 = 0;


void setup() {
	Wire.begin(CONTROL_SLAVE_DEVICE_ID);
	Wire.onRequest(requestCallback);

	mySerial.begin(38400);                                // 57600 = max value for softserial
	pinMode(ledPin, OUTPUT);

#ifdef DEBUG
	Serial.begin(115200);
#endif // DEBUG

	DEBUG_PRINTLN("Control started.");

	while (mySerial.available())  mySerial.read();         // empty RX buffer
}

void loop() {
	if (mySerial.available()) {                           // data received from smartphone
		delay(2);
		cmd[0] = mySerial.read();
		if (cmd[0] == STX) {
			int i = 1;
			while (mySerial.available()) {
				delay(1);
				cmd[i] = mySerial.read();
				if (cmd[i] > 127 || i > 7)                 break;     // Communication error
				if ((cmd[i] == ETX) && (i == 2 || i == 7))   break;     // Button or Joystick data
				i++;
			}
			if (i == 2)          getButtonState(cmd[1]);    // 3 Bytes  ex: < STX "C" ETX >
			else if (i == 7)          getJoystickState(cmd);     // 6 Bytes  ex: < STX "200" "180" ETX >
		}
	}
	sendBlueToothData();
}

void sendBlueToothData() {
	static long previousMillis = 0;
	long currentMillis = millis();
	if (currentMillis - previousMillis > sendInterval) {   // send data back to smartphone
		previousMillis = currentMillis;

		// Data frame transmitted back from Arduino to Android device:
		// < 0X02   Buttons state   0X01   DataField#1   0x04   DataField#2   0x05   DataField#3    0x03 >  
		// < 0X02      "01011"      0X01     "120.00"    0x04     "-4500"     0x05  "Motor enabled" 0x03 >    // example

		mySerial.print((char)STX);                                             // Start of Transmission
		mySerial.print(getButtonStatusString());  mySerial.print((char)0x1);   // buttons status feedback
		mySerial.print(GetdataInt1());            mySerial.print((char)0x4);   // datafield #1
		mySerial.print(GetdataFloat2());          mySerial.print((char)0x5);   // datafield #2
		mySerial.print(displayStatus);                                         // datafield #3
		mySerial.print((char)ETX);                                             // End of Transmission
	}
}

String getButtonStatusString() {
	String bStatus = "";
	for (int i = 0; i < 6; i++) {
		if (buttonStatus & (B100000 >> i))      bStatus += "1";
		else                                  bStatus += "0";
	}
	return bStatus;
}

int GetdataInt1() {              // Data dummy values sent to Android device for demo purpose
	static int i = -30;              // Replace with your own code
	i++;
	if (i > 0)    i = -30;
	return i;
}

float GetdataFloat2() {           // Data dummy values sent to Android device for demo purpose
	static float i = 50;               // Replace with your own code
	i -= .5;
	if (i < -50)    i = 50;
	return i;
}

void getJoystickState(byte data[8]) {
	int joyX = (data[1] - 48) * 100 + (data[2] - 48) * 10 + (data[3] - 48);       // obtain the Int from the ASCII representation
	int joyY = (data[4] - 48) * 100 + (data[5] - 48) * 10 + (data[6] - 48);
	joyX = joyX - 200;                                                  // Offset to avoid
	joyY = joyY - 200;                                                  // transmitting negative numbers

	if (joyX < -100 || joyX>100 || joyY < -100 || joyY>100)     return;      // commmunication error

	// Your code here ...

	xPosition1 = (joyX + 100) / 200.0 * 1024;
	yPosition1 = (joyY + 100) / 200.0 * 1024;
}

void getButtonState(int bStatus) {
	switch (bStatus) {
		// -----------------  BUTTON #1  -----------------------
	case 'A':
		buttonStatus |= B000001;        // ON
		DEBUG_PRINTLN("\n** Button_1: ON **");
		// your code...      
		displayStatus = "Button_1 <ON>";
		DEBUG_PRINTLN(displayStatus);
		digitalWrite(ledPin, HIGH);
		break;
	case 'B':
		buttonStatus &= B111110;        // OFF
		DEBUG_PRINTLN("\n** Button_1: OFF **");
		// your code...      
		displayStatus = "Button_1 <OFF>";
		DEBUG_PRINTLN(displayStatus);
		digitalWrite(ledPin, LOW);
		break;

		// -----------------  BUTTON #2  -----------------------
	case 'C':
		buttonStatus |= B000010;        // ON
		DEBUG_PRINTLN("\n** Button_2: ON **");
		// your code...      
		displayStatus = "Button2 <ON>";
		DEBUG_PRINTLN(displayStatus);
		break;
	case 'D':
		buttonStatus &= B111101;        // OFF
		DEBUG_PRINTLN("\n** Button_2: OFF **");
		// your code...      
		displayStatus = "Button2 <OFF>";
		DEBUG_PRINTLN(displayStatus);
		break;

		// -----------------  BUTTON #3  -----------------------
	case 'E':
		buttonStatus |= B000100;        // ON
		DEBUG_PRINTLN("\n** Button_3: ON **");
		// your code...      
		displayStatus = "Button_3 <ON>"; // Demo text message
		DEBUG_PRINTLN(displayStatus);
		break;
	case 'F':
		buttonStatus &= B111011;      // OFF
		DEBUG_PRINTLN("\n** Button_3: OFF **");
		// your code...      
		displayStatus = "Button_3 <OFF>";
		DEBUG_PRINTLN(displayStatus);
		break;

		// -----------------  BUTTON #4  -----------------------
	case 'G':
		buttonStatus |= B001000;       // ON
		DEBUG_PRINTLN("\n** Button_4: ON **");
		// your code...      
		displayStatus = "Datafield update <FAST>";
		DEBUG_PRINTLN(displayStatus);
		sendInterval = FAST;
		break;
	case 'H':
		buttonStatus &= B110111;    // OFF
		DEBUG_PRINTLN("\n** Button_4: OFF **");
		// your code...      
		displayStatus = "Datafield update <SLOW>";
		DEBUG_PRINTLN(displayStatus);
		sendInterval = SLOW;
		break;

		// -----------------  BUTTON #5  -----------------------
	case 'I':           // configured as momentary button
						//      buttonStatus |= B010000;        // ON
		DEBUG_PRINTLN("\n** Button_5: ++ pushed ++ **");
		// your code...      
		displayStatus = "Button5: <pushed>";
		break;
		//   case 'J':
		//     buttonStatus &= B101111;        // OFF
		//     // your code...      
		//     break;

		// -----------------  BUTTON #6  -----------------------
	case 'K':
		buttonStatus |= B100000;        // ON
		DEBUG_PRINTLN("\n** Button_6: ON **");
		// your code...      
		displayStatus = "Button6 <ON>"; // Demo text message
		break;
	case 'L':
		buttonStatus &= B011111;        // OFF
		DEBUG_PRINTLN("\n** Button_6: OFF **");
		// your code...      
		displayStatus = "Button6 <OFF>";
		break;
	}
	// ---------------------------------------------------------------
}

void requestCallback()
{
	TRACE_PRINTLN("Request recieved.");

	int buttonState1 = buttonStatus&B000001 ? 0 : 1024;
	int buttonState2 = buttonStatus&B000010 ? 0 : 1024;
	int buttonState3 = buttonStatus&B000100 ? 0 : 1024;
	int buttonState4 = buttonStatus&B001000 ? 0 : 1024;

	TRACE_PRINT("\tX: \t");
	TRACE_PRINT(xPosition1);
	TRACE_PRINT(";\tY: \t");
	TRACE_PRINT(yPosition1);
	TRACE_PRINTLN(";");

	uint8_t buffer[12];
	buffer[0] = yPosition1 >> 8;
	buffer[1] = yPosition1 & 0xff;
	buffer[2] = xPosition1 >> 8;
	buffer[3] = xPosition1 & 0xff;
	buffer[4] = buttonState1 >> 8;
	buffer[5] = buttonState1 & 0xff;
	buffer[6] = buttonState2 >> 8;
	buffer[7] = buttonState2 & 0xff;
	buffer[8] = buttonState3 >> 8;
	buffer[9] = buttonState3 & 0xff;
	buffer[10] = buttonState4 >> 8;
	buffer[11] = buttonState4 & 0xff;
	Wire.write(buffer, 12);
}
